<?php
declare(strict_types=1);

namespace gicaphp;

class Table
{
    private $firstNumber = 0;
    private $secondNumber = 0;
    private $arrayTable = [];
    
    public function __construct()
    {

        if ($this->isCli()) {
            $this->cliForm();
        } else {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $this->firstNumber = round($_POST['firstNumber']);
                $this->secondNumber = round($_POST['secondNumber']);
            }
        }

        if ($this->validInputs()) {
            $this->arrayTable = $this->resultsArray();
        }
    }

    /**
     * check if request is CLI
     *
     * @return boolean
     */
    public function isCli()
    {
        if (PHP_SAPI === 'cli') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * ask user to insert number in CLI
     *
     * @return CLI prompt for each number
     */
    private function cliForm()
    {
        $this->firstNumber = round(readline('Input 1st number: '));
        $this->secondNumber = round(readline('Input 2nd number: '));
    }

    /**
     * check if both inputs are valid inputs
     *
     * @return boolean
     */
    private function validInputs()
    {
        if ($this->firstNumber >= 1 && $this->secondNumber >= 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * create an array of multiplication results
     * and set it as multidimensional array
     *
     * @param set array
     */
    public function resultsArray()
    {
        $array = [];
        for ($i = 1; $i <= $this->firstNumber; $i++) {
            for ($ii = 1; $ii <= $this->secondNumber; $ii++) {
                $result = $i * $ii;
                $array[$i][] = $result;
            }
        }
        return $array;
    }

    /**
     * create CLI table
     *
     * @return table
     */
    public function cliTable()
    {
        $padLength = strlen((string)($this->firstNumber * $this->secondNumber));

        // shell commands
        $setTextBold = `tput setab 1`;
        $setTextDefault = `tput sgr0`;

        // set 1st row bold
        $table = $setTextBold;
        $table .= $this->addSpaces(' ') . ' ';
        
        for ($i = 1; $i <= $this->secondNumber; $i++) {
            $table .= $this->addSpaces($i) . ' ';
        }

        $table .= $setTextDefault;

        $table .= "\n";

        foreach ($this->arrayTable as $row) {
            // make 1st col of each row bold
            $table .= $setTextBold;
            $table .= $this->addSpaces($row[0]) . ' ';
            // remove any formatting
            $table .= $setTextDefault;
            foreach ($row as $col) {
                $table .= $this->addSpaces($col) . ' ';
            }
            $table .= "\n";
        }
        return $table;
    }

    /**
     * prepare html table
     *
     * @return html table
     */
    public function htmlTable()
    {
        $table = '<table>';
    
        $table .= '<tr>';
        $table .= '<td>' . $this->addSpaces(' ') . '</td>';
        for ($i = 1; $i <= $this->secondNumber; $i++) {
            $table .= '<td>' . $this->addSpaces($i) . '</td>';
        }
        $table .= '</tr>';
        
        foreach ($this->arrayTable as $row) {
            $table .= '<tr>';
            $table .= '<td>' . $this->addSpaces($row[0]) . '</td>';
            foreach ($row as $col) {
                $table .= '<td>' . $this->addSpaces($col) . '</td>';
            }
            $table .= '</tr>';
        }
        
        $table .= '</table>';

        return $table;
    }

    /**
     * add spaces for alignment
     *
     * @return new value with added spaces
     */
    private function addSpaces($val)
    {
        $lengthCell = strlen((string)($this->firstNumber * $this->secondNumber));
        $newVal = str_pad((string)$val, $lengthCell, ' ', STR_PAD_LEFT);
        return $newVal;
    }
}
