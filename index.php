<?php

declare(strict_types=1);

include_once 'class_timestable.php';

$obj = new gicaphp\Table();

$array = $obj->resultsArray();

$cli = $obj->isCli();


if ($cli) {
    if (count($array) > 0) {
        echo $obj->cliTable();
    }
    die;
} else {
    if (count($array) > 0) {
        $table = $obj->htmlTable();
    } else {
        $table = '';
    }
}

include 'page.php';
