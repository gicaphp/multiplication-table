<?php

declare(strict_types=1);

?><!DOCTYPE html>
<html  lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Multiplication Table</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <h1>Multiplication Table</h1>
        
        <form action="<?=$_SERVER['PHP_SELF']?>" method="post">
            Input 1st number:<br>
            <input type="number" min="1" max="99" name="firstNumber"><br>
            Input 2nd number:<br>
            <input type="number" min="1" max="99" name="secondNumber"><br>
            <br>
            <input type="submit" value="Generate">
        </form>

        <?php echo $table ?>
        
    </body>
</html>