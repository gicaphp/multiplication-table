# Multiplication Table

Create a times table using one or two numbers between 1 and 99.

Files used: 

* index.php
* class_timestable.php
* style.css

Available in CLI and Browser.
